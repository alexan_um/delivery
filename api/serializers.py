from django.contrib.auth.models import User, Group
from rest_framework import serializers
from api.models import Product, Package, Profile, UserStats, ProductStats

class UserSerializer(serializers.HyperlinkedModelSerializer):
    middle_name = serializers.CharField(source='profile.middle_name', allow_blank=True)
    address = serializers.CharField(source='profile.address', allow_blank=True)
    photo = serializers.CharField(source='profile.photo', allow_blank=True)
    
    class Meta:
        model = User
        fields = ('url', 'username', 'last_name', 'first_name', 'middle_name', 'email', 'address', 'photo')
        
    def create(self, validated_data):
        profile_data = validated_data.pop('profile', None)
        whoami = super(UserSerializer, self).create(validated_data)
        self.update_or_create_profile(whoami, profile_data)
        return whoami

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile', None)
        res = super(UserSerializer, self).update(instance, validated_data)
        self.update_or_create_profile(instance, profile_data)
        return res
    
    def update_or_create_profile(self, whoami, profile_data):
        Profile.objects.update_or_create(whoami=whoami, defaults=profile_data)

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile

class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        
        
class PackageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Package
        
        
class UserStatsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserStats

class ProductStatsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProductStats
