from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

class Profile(models.Model):
    whoami = models.OneToOneField(User, on_delete=models.CASCADE)
    middle_name = models.CharField(max_length=20, blank=True)
    address = models.CharField(max_length=200, blank=True)
    photo = models.CharField(max_length=200, blank=True)



class Package(models.Model):
    state_of_pack = (('S','Send'),('D','Delivery'),('R','Return'))
    
    recipient = models.ForeignKey(User)
    departure_date = models.DateField('date departure')
    delivery_date = models.DateField('date delivery')
    status = models.CharField(max_length=1, choices=state_of_pack)
    value = models.IntegerField(default=0)
    date_transmit = models.DateField('date transmit')

class Product(models.Model):
    article = models.IntegerField(default=0)
    manufacture = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    barcode = models.IntegerField(default=0)
    photo = models.CharField(max_length=200)
    cost = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    package = models.ForeignKey(Package)


class UserStats(models.Model):
    user = models.ForeignKey(User)
    chance = models.IntegerField(default=100)


class ProductStats(models.Model):
    product = models.ForeignKey(Product)
    


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(whoami=instance)
        Token.objects.create(user=instance)
