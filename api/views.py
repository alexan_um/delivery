from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import Http404
from api.serializers import *
from api.models import Product, Package, Profile, UserStats
from rest_framework import viewsets


import datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

#from rest_framework.decorators import api_view

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined').select_related('profile')
    serializer_class = UserSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    
    
class PackageViewSet(viewsets.ModelViewSet):
    queryset = Package.objects.all()
    serializer_class = PackageSerializer

class UserStatsViewSet(viewsets.ModelViewSet):
    queryset = UserStats.objects.all()
    serializer_class = UserStatsSerializer
    
    
class ProductStatsViewSet(viewsets.ModelViewSet):
    queryset = ProductStats.objects.all()
    serializer_class = ProductStatsSerializer
